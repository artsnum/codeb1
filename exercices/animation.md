---
title: Animez vos formes
lang: fr-FR
---

# Animez vos formes

Un exercice simple pour vous familiariser avec l'interface, la syntaxe et les fonctions de base de Processing.