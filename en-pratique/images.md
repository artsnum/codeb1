---
title: Travailler avec des images
lang: fr-FR
---

# Travailler avec des images

## Importer une image

Les images que vous utilisez doivent se trouver dans un dossier **data** situé dans le dossier de votre sketch. C'est là que Processing ira les chercher par défaut.

```processing
PImage img; // déclaration de la variable image

void setup() {
    size(600, 600);    
    // L'image doit se situer dans un dossier appelé data dans le dossier de votre sketch Processing.
    img = loadImage("workshop.jpg"); // attention, pas de caractères spéciaux ni accents ni espaces
}

void draw() {
    image(img, 0, 0); // on affiche l'image en haut à gauche de la page.
}
```

**Note : ** Essayez maintenant de faire en sorte que l'image suive votre souris

<a data-fancybox title="" href="/assets/image-souris.png">![](/assets/image-souris.png)</a>

## Redimensionner une image

```processing
PImage img; // déclaration de la variable image

void setup() {
    size(600, 600);    
    img = loadImage("workshop.jpg");
}

void draw() {
    image(img, 0, 0, width/2, height/2);
}
```

<a data-fancybox title="" href="/assets/image-resize.png">![](/assets/image-resize.png)</a>

<a data-fancybox title="" href="/assets/image-rouge.png">![](/assets/image-rouge.png)</a>


## Changer la teinte de l'image

```processing
PImage img;

void setup() {
    size(600, 600);    
    img = loadImage("workshop.jpg");
}

void draw() {
    tint(255, 0, 0, 128); // teinte en rouge et opacité à 50%
    image(img, 0, 0);
}
```

----

### En savoir plus :

+ https://fr.flossmanuals.net/processing/les-images/
+ https://processing.org/tutorials/pixels/
+ https://clickandwatch.net/pixel-sorting-experiments
+ https://medium.com/@xoalexo/pixel-sorting-is-the-coolest-thing-youve-never-tried-48097954e9f3